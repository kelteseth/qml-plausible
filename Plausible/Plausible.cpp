#include "Plausible.h"

#include <QDate>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkReply>
#include <QNetworkRequest>

Plausible::Plausible()
{
    if (!m_settings.contains(m_settings_uuid_key)) {
        resetClientId();
    } else {
        const QString currentClientId = m_settings.value(m_settings_uuid_key).toString();
        if (!currentClientId.contains("___")) {
            // Id is invalid. See generateAnonymousClientId()
            resetClientId();
            return;
        }
        const QStringList clientIdData = currentClientId.split("___");
        const QDate generationDateTime = QDate::fromString(clientIdData.at(0), m_settings_dateFormat);
        // Keep the same session id for the same day
        if (generationDateTime == QDate::currentDate()) {
            setClientId(m_settings.value(m_settings_uuid_key).toString());
            return;
        }

        resetClientId();
    }
}

/*!
 * \brief Plausible::event
    Sends a plausible event
 */
bool Plausible::event(const QString& eventName, const QString& page, const QString& props, const QString& referrer)
{
    if (!m_enabled)
        return true;

    if (!referrer.isEmpty())
        m_referrer = referrer;

    const QString userAgent = getUserAgent();
    const QString url = m_serverUrl + "/api/event";

    QNetworkRequest request;
    request.setHeader(QNetworkRequest::KnownHeaders::UserAgentHeader, userAgent);
    request.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader, "application/json");
    request.setRawHeader("X-Forwarded-For", "127.0.0.1");
    request.setUrl(url);

    // https://plausible.io/docs/events-api
    QJsonObject body;
    // Name of the event. Can specify pageview which is a special type of event in Plausible.
    // All other names will be treated as custom events.
    body.insert("name", eventName);

    // The URL parameter will feel strange in a mobile app but you can manufacture
    // something that looks like a web URL. If you name your mobile app screens
    // like page URLs, Plausible will know how to handle it. So for example,
    // on your login screen you could send something like:
    // event: pageview
    // url: app://localhost/login
    body.insert("url", "app://localhost/" + m_domain + "/" + page);

    // Domain name of the site in Plausible
    // Note: This is the domain name you used when you added your site to your Plausible account.
    // It doesn't need to be an actual domain name, so when adding your mobile app to Plausible,
    // you could insert the mobile app name in the domain name field
    body.insert("domain", m_domain);

    // Referrer for this event. When using the script, this is set to document.referrer
    body.insert("referrer", QString(referrer.isEmpty() ? m_referrer : referrer));

    // Width of the screen. When using the script, this is set to window.innerWidth
    body.insert("screen_width", m_screenSize.width());

    // Add custom properties we have any
    if (!props.isEmpty()) {
        QJsonObject obj;
        QJsonParseError err {};
        QJsonDocument doc = QJsonDocument::fromJson(props.toUtf8(), &err);

        if (err.error == QJsonParseError::NoError) {
            body.insert("props", doc.object());
        } else {
            qWarning() << " Invalid custom prop set. This must be a valid json obiect. Use: JSON.parse(JsonString) in QML.";
        }
    }

    const QByteArray data = QJsonDocument(body).toJson();

    QNetworkReply* reply = m_networkAccessManager.post(request, data);
    const QString log = "\nREQUEST:\nData:" + data + "\nUrl: " + url + "\nuserAgent: " + userAgent;
    appendLog(log);

    if (m_debug)
        qInfo() << log;

    QObject::connect(reply, &QNetworkReply::readyRead, this, [this, reply]() {
        const QByteArray data = reply->readAll();
        if (data.size() <= 0) {
            return;
        }
        if (m_debug)
            qInfo() << "readyRead: " << data;
    });
    QObject::connect(reply, &QNetworkReply::finished, this, [this]() {
        if (m_debug)
            qInfo() << "finished!";
    });

    QObject::connect(reply, &QNetworkReply::errorOccurred, this, []() {
        qInfo() << "errorOccurred!";
    });

    return true;
}

/*!
 * \brief Plausible::pageView
    Can specify pageview which is a special type of event in Plausible. All other names will be treated as custom events.
 */
bool Plausible::pageView(const QString& page, const QString& referrer)
{
    if (!referrer.isEmpty())
        m_referrer = referrer;

    return event("pageview", page, referrer);
}

/*!
 * \brief Plausible::resetClientId
    To anonymously detect different clients we generate a QUuid.
 */
const QString Plausible::resetClientId()
{
    setClientId(generateAnonymousClientId());
    m_settings.setValue(m_settings_uuid_key, m_clientId);
    m_settings.sync();
    return m_clientId;
}

/*!
 * \brief Plausible::getUserAgent
 * Generates a fake UserAgents like browsers have. Set it to a none existing Firefox version.
 */
const QString Plausible::getUserAgent() const
{
    const QString os = QSysInfo::productType();
    const QString osVersion = QSysInfo::productVersion();
    const QString osArch = QSysInfo::buildAbi();

    QString osUserAgent;
    // Lets set it to a unrealistic firefox version, because we _must_ use a browser useragent.
    QString userAgent = "Mozilla/5.0 (%1; rv:10000) Gecko/10000 Firefox/10000 uuid/" + clientId();

    if (os == "windows") {
        osUserAgent = "Windows NT " + osVersion + "; Win64; x64;";
        return userAgent.arg(osUserAgent);
    }

    if (os == "macos" || os == "ios" || os == "darwin") {
        if (osArch == "x86_64") {
            osUserAgent = "Macintosh; M1 Mac OS X " + osVersion;
        } else if (osArch == "arm") {
            osUserAgent = "Macintosh; Intel Mac OS X " + osVersion;
        }

        return userAgent.arg(osUserAgent);
    }

    if (os == "linux" || os == "android") {
        osUserAgent = "X11; Linux " + osArch;

        return userAgent.arg(osUserAgent);
    }

    return userAgent.arg("UNKNOWN");
}

/*!
 * \brief Plausible::getUserAgent
 * See: https://plausible.io/docs/custom-event-goals#using-custom-props
 * Note that you must ensure that no personally identifiable information (PII) is sent to Plausible Analytics with
 * custom properties. PII is information that could be used on its own to identify, contact, or precisely locate
 * an individual. This includes:\
 * [...]
 * > pseudonymous cookie IDs, advertising IDs or other pseudonymous end user identifiers
 * Add current date so we regemerate a new id every day.
 */
const QString Plausible::generateAnonymousClientId() const
{
    return QDate().currentDate().toString(m_settings_dateFormat) + "___" + QUuid::createUuid().toString();
}
