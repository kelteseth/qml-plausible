import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Controls.Material
import Plausible 1.0

Window {
    id: root
    width: 1600
    height: 860
    visible: true
    Material.theme: Material.System
    title: qsTr("Plausible EU Private Analytics Demo App")

    Plausible {
        id: plausible
        screenSize: Qt.size(root.width, root.height)
        domain: tfDomain.text
        serverUrl: tfPlausibleServer.text
        debug: true
    }

    Component.onCompleted: {
        plausible.pageView("page-" + view.currentIndex)
    }

    SplitView {
        anchors.fill: parent
        Rectangle {
            z: 2
            SplitView.preferredWidth: parent.width * .3
            SplitView.fillHeight: true
            color: Material.background
            ColumnLayout {
                anchors.fill: parent
                anchors.margins: 20
                spacing: 20
                Text {
                    color: Material.primaryTextColor
                    text: qsTr("Custom Events (Goals)")
                    font.pointSize: 16
                    Layout.fillWidth: true
                    horizontalAlignment: Text.AlignHCenter
                }
                Text {
                    color: Material.secondaryTextColor
                    text: qsTr("IMPORTANT: You must register these evens manually! <br>Site Settings -> Goals -> Add Goal -> Custom Event -> Add Goal.<br>See <a href=\"https://plausible.io/docs/custom-event-goals#2-create-a-custom-event-goal-in-your-plausible-analytics-account\">Plausible Docs</a>")
                    onLinkActivated: link => Qt.openUrlExternally(link)
                    linkColor: Material.color(Material.LightBlue)
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                    font.pointSize: 10
                    Layout.fillWidth: true
                    horizontalAlignment: Text.AlignHCenter
                }
                Item {
                    Layout.fillWidth: true
                    Layout.preferredHeight: 20
                }
                Text {
                    color: Material.primaryTextColor
                    text: qsTr("Events with custom (JSON) data<br>See <a href=\"https://plausible.io/docs/custom-event-goals#using-custom-props\">Plausible Docs</a>")
                    onLinkActivated: link => Qt.openUrlExternally(link)
                    linkColor: Material.color(Material.LightBlue)
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                    font.pointSize: 14
                    Layout.fillWidth: true
                    horizontalAlignment: Text.AlignHCenter
                }
                Button {
                    onClicked: {
                        // We cannot directly send json to c++ (wtf Qt is is 2022)
                        const jsonString = '{"Property 1" : "What you guys are referring to as Linux", "Property 2" : "is in fact, GNU/Linux. 😅"}'
                        plausible.event("Test Event 1",
                                        "page-" + view.currentIndex, jsonString)
                    }

                    text: "Trigger event: `Test Event 1` (With custom json properties)"
                    Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
                }
                Text {
                    color: Material.primaryTextColor
                    text: qsTr("Regular events:")
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                    font.pointSize: 14
                    Layout.fillWidth: true
                    horizontalAlignment: Text.AlignHCenter
                }
                Button {
                    onClicked: plausible.event("Test Event 2",
                                               "page-" + view.currentIndex)
                    text: "Trigger event: `Test Event 2`"
                    Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
                }
                Button {
                    onClicked: plausible.event("Test Event 3",
                                               "page-" + view.currentIndex)
                    text: "Trigger event: `Test Event 3`"
                    Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
                }
                Button {
                    onClicked: plausible.event("Test Event 4",
                                               "page-" + view.currentIndex)
                    text: "Trigger event: `Test Event 4`"
                    Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
                }
                Item {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                }
                Text {
                    color: Material.secondaryTextColor
                    text: qsTr("Plausible Server")
                    Layout.fillWidth: true
                }
                TextField {
                    id: tfPlausibleServer
                    text: "https://plausible.io"
                    Layout.fillWidth: true
                }
                Text {
                    color: Material.secondaryTextColor
                    text: qsTr("Your Domain")
                    Layout.fillWidth: true
                }
                TextField {
                    id: tfDomain
                    text: "app.screen-play.app"
                    Layout.fillWidth: true
                }

                Button {
                    highlighted: true
                    onClicked: Qt.openUrlExternally(
                                   plausible.serverUrl + "/" + plausible.domain)
                    text: "Open Plausible Webinterace"
                    Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
                }
            }
        }

        Item {
            SplitView.fillHeight: true
            SplitView.preferredWidth: parent.width * .7
            SwipeView {
                id: view
                anchors.fill: parent
                onCurrentIndexChanged: {
                    plausible.pageView("page-" + view.currentIndex)
                }

                Rectangle {
                    color: Material.color(Material.Blue, Material.Shade900)
                }
                Rectangle {
                    color: Material.color(Material.Blue, Material.Shade800)
                }
                Rectangle {
                    color: Material.color(Material.Blue, Material.Shade700)
                }
                Rectangle {
                    color: Material.color(Material.Blue, Material.Shade600)
                }
                Rectangle {
                    color: Material.color(Material.Blue, Material.Shade500)
                }
                Rectangle {
                    color: Material.color(Material.Blue, Material.Shade400)
                }
            }
            ColumnLayout {
                id: topLayout
                anchors.top: parent.top
                anchors.topMargin: 20
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: 20
                Text {
                    text: "Swipe to trigger: plausible.pageView(\"page-\" + view.currentIndex)"
                    color: "white"
                    font.pointSize: 16
                    Layout.alignment: Qt.AlignHCenter
                }
            }
            Item {
                anchors {
                    top: topLayout.bottom
                    right: parent.right
                    left: parent.left
                    topMargin: 50
                    margins: 100
                    bottomMargin: 100
                    bottom: bottomLayout.top
                }
                Rectangle {
                    anchors.fill: parent
                    opacity: .8
                }
                ScrollView {
                    id: logWrapper
                    anchors.fill: parent
                    anchors.margins: 20
                    ScrollBar.horizontal: ScrollBar {}
                    Text {
                        text: plausible.log
                        width: logWrapper.width
                        wrapMode: Text.Wrap
                    }
                }
            }

            ColumnLayout {
                id: bottomLayout
                anchors.bottom: view.bottom
                anchors.bottomMargin: 20
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: 20
                Button {
                    text: "Current ClientId : " + plausible.clientId + ". Click to reset"
                    onClicked: plausible.resetClientId()
                    Layout.alignment: Qt.AlignHCenter
                }
                Text {
                    text: "Current Page Index: " + indicator.currentIndex
                    Layout.alignment: Qt.AlignHCenter
                    color: "white"
                }

                PageIndicator {
                    id: indicator
                    Layout.alignment: Qt.AlignHCenter
                    count: view.count
                    currentIndex: view.currentIndex
                }
            }
        }
    }
}
