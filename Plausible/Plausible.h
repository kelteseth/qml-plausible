#pragma once
#include <QDebug>
#include <QObject>
#include <QQmlEngine>
#include <QSettings>
#include <QSize>
#include <QString>
#include <QUrl>
#include <QVariant>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>

#include <memory>

class Plausible : public QObject {
    Q_OBJECT
    QML_ELEMENT
    Q_PROPERTY(QString serverUrl READ serverUrl WRITE setServerUrl NOTIFY serverUrlChanged)
    Q_PROPERTY(QString userAgent READ userAgent WRITE setUserAgent NOTIFY userAgentChanged)
    Q_PROPERTY(QString domain READ domain WRITE setDomain NOTIFY domainChanged)
    Q_PROPERTY(QSize screenSize READ screenSize WRITE setScreenSize NOTIFY screenSizeChanged)
    Q_PROPERTY(QString clientId READ clientId WRITE setClientId NOTIFY clientIdChanged)
    Q_PROPERTY(QString log READ log WRITE setLog NOTIFY logChanged)
    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)
    Q_PROPERTY(bool debug READ debug WRITE setDebug NOTIFY debugChanged)

public:
    explicit Plausible();

    const QString& serverUrl() const { return m_serverUrl; }
    const QString& userAgent() const { return m_userAgent; }
    const QString& domain() const { return m_domain; }
    const QSize& screenSize() const { return m_screenSize; }
    const QString& clientId() const { return m_clientId; }
    bool enabled() const { return m_enabled; }
    bool debug() const { return m_debug; }
    const QString& log() const { return m_log; }

public slots:
    bool event(const QString& eventName, const QString& page, const QString& props = "", const QString& referrer = "");
    bool pageView(const QString& page, const QString& referrer = "");
    const QString resetClientId();

public slots:
    void setServerUrl(const QString& serverUrl)
    {
        if (m_serverUrl == serverUrl)
            return;
        m_serverUrl = serverUrl;
        emit serverUrlChanged(m_serverUrl);
    }
    void setUserAgent(const QString& userAgent)
    {
        if (m_userAgent == userAgent)
            return;
        m_userAgent = userAgent;
        emit userAgentChanged(m_userAgent);
    }

    void setDomain(const QString& domain)
    {
        if (m_domain == domain)
            return;
        m_domain = domain;
        emit domainChanged(m_domain);
    }

    void setScreenSize(const QSize& screenSize)
    {
        if (m_screenSize == screenSize)
            return;
        m_screenSize = screenSize;
        emit screenSizeChanged(m_screenSize);
    }

    void setEnabled(bool enabled)
    {
        if (m_enabled == enabled)
            return;
        m_enabled = enabled;
        emit enabledChanged(m_enabled);
    }

    void setDebug(bool debug)
    {
        if (m_debug == debug)
            return;
        m_debug = debug;
        emit debugChanged(m_debug);
    }

    void setClientId(const QString& clientId)
    {
        if (m_clientId == clientId)
            return;
        m_clientId = clientId;
        emit clientIdChanged(m_clientId);
    }

    void setLog(const QString& log)
    {
        if (m_log == log)
            return;
        m_log = log;
        emit logChanged(m_log);
    }
    void appendLog(const QString& log)
    {
        if (m_log.size() > 10000) {
            m_log = "###### LOG CLEARED ######";
        }
        m_log += log;
        emit logChanged(m_log);
    }

private:
    const QString getUserAgent() const;
    const QString generateAnonymousClientId() const;

signals:
    void serverUrlChanged(const QString& serverUrl);
    void userAgentChanged(const QString& userAgent);
    void domainChanged(const QString& domain);
    void screenSizeChanged(const QSize& screenSize);
    void enabledChanged(bool enabled);
    void debugChanged(bool debug);
    void clientIdChanged(const QString& clientId);
    void logChanged(const QString& log);

private:
    QString m_serverUrl = "https://plausible.io";
    QString m_ipv4Address;
    QString m_userAgent;
    QString m_domain;
    QSize m_screenSize;
    QString m_referrer;
    QString m_clientId;
    QSettings m_settings;
    const QString m_settings_uuid_key = "plausible-uuid";
    const QString m_settings_dateFormat = "ddMMyyyy";
    bool m_enabled = true;
    bool m_debug = false;
    QNetworkAccessManager m_networkAccessManager;
    QString m_log;
};
