#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QVersionNumber>
#include <QtQml/qqmlextensionplugin.h>

Q_IMPORT_QML_PLUGIN(PlausiblePlugin)

int main(int argc, char* argv[])
{

    QGuiApplication app(argc, argv);
    QGuiApplication::setOrganizationName("QMLPlausibleTestApp");
    QGuiApplication::setOrganizationDomain("test.screen-play.app");
    QGuiApplication::setApplicationName("QMLPlausibleTestApp");
    QGuiApplication::setApplicationVersion(QVersionNumber(1, 0, 0).toString());

    QQmlApplicationEngine engine;
    // The first subfolder is the libraryName followed by the regular
    // folder strucutre:     LibararyName/Subfolder
    const QUrl url(u"qrc:/qt/qml/Plausible/Plausible/TestMain.qml"_qs);
    QObject::connect(
        &engine, &QQmlApplicationEngine::objectCreated,
        &app, [url](QObject* obj, const QUrl& objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        },
        Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
