# Qt6 QML Plausible Anonymous Analytics


<div align="center">
<img  src=".gitlab/media/plausible_logo_dark.webp">
</div>
<br>

> [Plausible](https://plausible.io/) is lightweight and open source web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. Made and hosted in the EU, powered by European-owned cloud infrastructure  🇪🇺 

This project lets you easily add private analytics to your project. It is dual licensed via MIT/Apache 2 at your choosing.

![](.gitlab/media/plausible_site.png)

It uses `url` to detect the current state of your app. From the [plausible docs](https://plausible.io/docs/events-api):

> The URL parameter will feel strange in a mobile app but you can manufacture something that looks like a web URL. If you name your mobile app screens like page URLs, Plausible will know how to handle it. So for example, on your login screen you could send something like:

```
event: pageview
url: app://localhost/login

The pathname (/login) is what will be shown as the page value in the Plausible dashboard. 
```

## Features
- ✅ __Privacy first__ with rotating client uuid (every 24h)
- ✅ Page visits
- ✅ Automatic refferer
- ✅ Custom events (Goals)
- ✅ Property events with custom (JSON) data
- ✅ Simple integration and example app (see below)

## CMake based installation 
1. Clone/[Download](https://gitlab.com/kelteseth/qml-plausible/-/archive/main/qml-plausible-main.zip) this project into a __subdirectory__ of your project. For example into a folder named `ThirdParty`
 - _Alternatively_ you can use the CMake `FetchContent_Declare` and  `FetchContent_MakeAvailable`. This downloads this repo into your build folder, but QtCreator autocomplete is missing and [a known issue](https://bugreports.qt.io/browse/QTCREATORBUG-27083).
 ``` cmake
FetchContent_Declare(
  qml-plausible
  GIT_REPOSITORY https://gitlab.com/kelteseth/qml-plausible.git
  GIT_TAG        34b8d3b898ba3a8a40db2c306e9907dadc2d49f7
)

FetchContent_MakeAvailable(qml-plausible)
```
2. Add *qml-plausible* as a subdirectory
``` cmake
    add_subdirectory(ThirdParty/qml-plausible)
```
3. Add `Plausibleplugin` to your project (yes that is a lowercase `plugin`). 

``` cmake
    target_link_libraries(YourAppName PRIVATE Qt6::Quick Plausibleplugin)
```
4. Add PlausiblePlugin to your main.cpp
``` cpp
#include <QtQml/qqmlextensionplugin.h>

Q_IMPORT_QML_PLUGIN(PlausiblePlugin)

int main(int argc, char* argv[])
{ //...
``` 
5. Create a plausible account and add your site.
    1. Because we have an app and not a website we must still set a `domain` at plausible.io
    2. Change the domain property to your newly created domain
6. Using plausible in your app
``` qml
import QtQuick
import QtQuick.Controls
import Plausible 1.0

Window {
    id: root
    width: 1366
    height: 768
    visible: true
    title: qsTr("Plausible EU Private Analytics")

    Plausible {
        id: plausible
        screenSize: Qt.size(root.width, root.height)
        domain: "app.this-is-my-app-domain.app" // Change this to your domain
        debug: true
    }

    Component.onCompleted:  plausible.pageView("MainPage")

    Button {
        onClicked: plausible.event("eventName", "page", "referrer") // referrer is the previous page
        text: "Trigger event"
    }
}
```


## Running the example app
Clone this repo and change the option in your QtCreator settings or directly in the `CMakeLists.txt`
``` cmake
option(TestProject "Builds TextProject" ON)
```

![](.gitlab/media/test_project.png)

## FAQ
1. Why does my app show up as a browser?
- This is a plausible limitation, because they only support browser at the moment. We set the UserAgent to Firefox version 10000 that you can filter in your dashboard.
2. Why MIT/Apache 2.0 dual license?
- Apache 2.0 is the better license but not compatible with GPLv2+. So when using this project in GPL projects you are using the MIT version.
3. Will there be a Qt5 version?
- No.
4. Do you accept contributions?
- Yes.
